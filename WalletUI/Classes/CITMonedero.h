//
//  Monedero.h
//  ExampleHTTPRequest
//
//  Created by CITA on 1/23/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CITMonedero : NSObject

@property (nonatomic, copy) NSString *estatus;
@property int id_monedero;
@property int id_tipoMonedero;
@property double saldoIntransferible;
@property double saldoTransferible;
@property (nonatomic, copy) NSString *tipoMonedero;

- (instancetype) initWithDictionary: (NSDictionary *) objectDictionary;

@end

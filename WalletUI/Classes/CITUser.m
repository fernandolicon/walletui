//
//  User.m
//  WalletUI
//
//  Created by CITA on 1/27/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITUser.h"

@implementation CITUser

+ (void) setUserName: (NSString *) userName{
    [[NSUserDefaults standardUserDefaults] setObject:userName forKey:@"userName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *) getUserName{
    NSString *u = (NSString*) [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"];
    if (u == nil)
        u = @"";
    return u;
}

+ (void) setPassword: (NSString *) password{
    [[NSUserDefaults standardUserDefaults] setObject:password forKey:@"password"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *) getPassword{
    NSString *u = (NSString*) [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
    if (u == nil)
        u = @"";
    return u;
}

+ (void) resetData{
    [self setUserName:@""];
    [self setPassword:@""];
}

@end

//
//  ViewControllerEditor.h
//  WalletUI
//
//  Created by CITA on 2/14/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CITViewControllerEditor : NSObject

+ (void) changeColorofStatusBarforView: (UIViewController *) controller;
+ (void) changeNavigationBarColorforView: (UITableViewController *) controller;


@end

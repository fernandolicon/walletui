//
//  Cafeteria.h
//  ExampleHTTPRequest
//
//  Created by CITA on 1/24/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CITCafeteria : NSObject

@property (nonatomic, copy) NSString *description;
@property (nonatomic, copy) NSString *estatus;
@property  int id_Categoria;
@property (nonatomic, copy)  NSString *name;

- (instancetype) initWithDictionary: (NSDictionary *) objectDictionary;

@end

//
//  Transaction.h
//  ExampleHTTPRequest
//
//  Created by CITA on 1/23/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CITTransaction : NSObject

@property int id_Transaccion;
@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *time;
@property double amount;
@property (nonatomic, copy) NSString *doneBy;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *user;
@property (nonatomic, copy) NSString *concept;

- (instancetype) initWithDictionary: (NSDictionary *) objectDictionary;

@end

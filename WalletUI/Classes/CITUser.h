//
//  User.h
//  WalletUI
//
//  Created by CITA on 1/27/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CITUser : NSObject

+ (void) setUserName: (NSString *) userName;
+ (NSString *) getUserName;
+ (void) setPassword: (NSString *) psswrd;
+ (NSString *) getPassword;
+ (void) resetData;

@end

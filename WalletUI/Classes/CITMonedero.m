//
//  Monedero.m
//  ExampleHTTPRequest
//
//  Created by CITA on 1/23/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITMonedero.h"

@implementation CITMonedero

- (instancetype) initWithDictionary: (NSDictionary *) objectDictionary{
    if (self = [super init]){
        self.estatus = [objectDictionary objectForKey:@"estatus"];
        self.id_monedero = [[objectDictionary objectForKey:@"idMonedero"] intValue];
        self.id_tipoMonedero = [[objectDictionary objectForKey:@"idTipoMonedero"] intValue];
        self.saldoIntransferible = [[objectDictionary objectForKey:@"saldoIntransferible"] doubleValue];
        self.saldoTransferible = [[objectDictionary objectForKey:@"saldoTransferible"] doubleValue];
        self.tipoMonedero = [objectDictionary objectForKey:@"tipoMonedero"];
        
    }
    
    return self;
}

@end

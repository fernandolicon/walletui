//
//  Cafeteria.m
//  ExampleHTTPRequest
//
//  Created by CITA on 1/24/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITCafeteria.h"

@implementation CITCafeteria

- (instancetype) initWithDictionary: (NSDictionary *) objectDictionary{
    if (self = [super init]){
        self.description = [objectDictionary objectForKey:@"descripcion"];
        self.estatus = [objectDictionary objectForKey:@"estatus"];
        self.id_Categoria = [[objectDictionary objectForKey:@"idCategoria"] intValue];
        self.name = [objectDictionary objectForKey:@"nombre"];
    }
    
    return self;
}


@end

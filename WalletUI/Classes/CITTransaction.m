//
//  Transaction.m
//  ExampleHTTPRequest
//
//  Created by CITA on 1/23/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITTransaction.h"

@implementation CITTransaction

- (instancetype) initWithDictionary: (NSDictionary *) objectDictionary{
    if (self = [super init]){
        self.id_Transaccion = [[objectDictionary objectForKey:@"idTransaccion"] intValue];
        self.date = [[objectDictionary objectForKey:@"fecha"] substringToIndex:10];
        self.time = [[objectDictionary objectForKey:@"fecha"] substringFromIndex:11];
        self.amount = [[objectDictionary objectForKey:@"monto"] doubleValue];
        self.doneBy = [objectDictionary objectForKey:@"realizadoPor"];
        self.type = [objectDictionary objectForKey:@"tipo"];
        self.user = [objectDictionary objectForKey:@"usuario"];
        self.concept = [objectDictionary objectForKey:@"concepto"];
    }
    
    return self;
}

@end
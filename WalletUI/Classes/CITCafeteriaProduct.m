//
//  CafeteriaProduct.m
//  ExampleHTTPRequest
//
//  Created by CITA on 1/24/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITCafeteriaProduct.h"

@implementation CITCafeteriaProduct

- (instancetype) initWithDictionary: (NSDictionary *) objectDictionary{
    
    if (self = [super init]){
        self.cantidad = [[objectDictionary objectForKey:@"cantidad"] doubleValue];
        self.estatus = [objectDictionary objectForKey:@"estatus"];
        self.idConcepto = [[objectDictionary objectForKey:@"idConcepto"] intValue];
        self.name = [objectDictionary objectForKey:@"nombre"];
        self.price = [[objectDictionary objectForKey:@"precio"] doubleValue];
    }
    
    return self;
}

@end

//
//  ViewControllerEditor.m
//  WalletUI
//
//  Created by CITA on 2/14/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITViewControllerEditor.h"

@implementation CITViewControllerEditor

+ (void) changeColorofStatusBarforView:(UIViewController *)controller{
    UIView *colorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
    colorView.backgroundColor = [UIColor colorWithRed:1.0/255.0f green:136.0/255.0f blue:199.0/255.0f alpha:1.0];
    [controller.view addSubview:colorView];
}

+ (void) changeNavigationBarColorforView: (UITableViewController *) controller{
    controller.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1.0/255.0f green:136.0/255.0f blue:199.0/255.0f alpha:1.0];
    [controller.navigationController.navigationBar setTranslucent:NO];
    [controller.navigationController.navigationBar setOpaque:YES];
    controller.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    controller.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    controller.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
}

@end

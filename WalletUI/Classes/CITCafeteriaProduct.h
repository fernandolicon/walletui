//
//  CafeteriaProduct.h
//  ExampleHTTPRequest
//
//  Created by CITA on 1/24/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CITCafeteriaProduct : NSObject

@property double cantidad;
@property (nonatomic, copy) NSString *estatus;
@property int idConcepto;
@property (nonatomic, copy) NSString *name;
@property double price;

- (instancetype) initWithDictionary: (NSDictionary *) objectDictionary;

@end

//
//  AjustesViewController.m
//  WalletUI
//
//  Created by CITA on 1/30/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITAjustesViewController.h"

@interface CITAjustesViewController ()

@end

@implementation CITAjustesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    items = [[NSArray alloc] initWithObjects:@"Desconectar", nil];
    
    [CITViewControllerEditor changeColorofStatusBarforView:self];
    
    tblAjustes.separatorColor = [UIColor colorWithRed:209/255.f green:198/255.f blue:181/255.f alpha:1];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark LogOut

- (void) performLogOut{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Desconectar" message:@"¿Estas seguro?" delegate:self cancelButtonTitle:@"Si" otherButtonTitles:@"No", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [CITUser resetData];

        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UITabBarController *myVC = (UITabBarController *)[storyboard instantiateViewControllerWithIdentifier:@"LogIn"];
        [self presentViewController:myVC animated:YES completion:nil];
    }
    if (buttonIndex == 1)
    {
        //Code for download button
    }
}

#pragma -mark TableView

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return items.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ajustesCell"];
    
    if ( cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ajustesCell"];
    }
    
    cell.textLabel.text = [items objectAtIndex:indexPath.row];
    
    //cell.textLabel.font = [UIFont fontWithName:@"Lane - Narrow" size:17];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            [self performLogOut];
            break;
        default:
            break;
    }
}


@end

//
//  CafeteriasViewController.h
//  WalletUI
//
//  Created by CITA on 1/30/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CITCafeteria.h"
#import "CITCafeteriaRequesting.h"
#import "CITCafeteriaItemsViewController.h"
#import "CITViewControllerEditor.h"

@interface CITCafeteriasViewController : UITableViewController{
    NSArray *cafeteriasArray;
    UIApplication *app;
    CITCafeteria *selectedCafeteria;
}

- (void) successfulConnectionWithArrayofObjects:(NSArray *)arrayCafeterias;
-(void) errorinConnection;

@end

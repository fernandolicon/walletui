//
//  ShoppingCartViewController.h
//  WalletUI
//
//  Created by CITA on 1/31/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CITProductCell.h"
#import "CITUser.h"
#import "CITCafeteriaProduct.h"
#import "CITPaymentRequesting.h"
#import "CITViewOrderViewController.h"
#import "CITCancelOrderRequesting.h"
#import "CITPendingOrderRequesting.h"

@interface CITShoppingCartViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>{
    
    __weak IBOutlet UILabel *txtTotal;
    
    __weak IBOutlet UITableView *tblView;
    double total;
    
    int pendingOrderID;
    
    UIApplication *networkIndicator;
    
    int idPay;
    __weak IBOutlet UIButton *btnOrder;
}

@property (nonatomic, strong) NSMutableArray *shoppingCart;
@property (nonatomic, strong) CITCafeteria *selectedCafeteria;

- (void) performOrder;
- (void) successfulPaymentwithId: (int) idPayment;
- (void) pendingOrder: (int) pendingId;
- (void) orderCancelled;
- (void) errorinConnection;

- (IBAction)buyCart:(id)sender;

@end

//
//  ShoppingCartViewController.m
//  WalletUI
//
//  Created by CITA on 1/31/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITShoppingCartViewController.h"

@interface CITShoppingCartViewController ()

@end

@implementation CITShoppingCartViewController

@synthesize shoppingCart;
@synthesize selectedCafeteria;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    //Addition in order to know the total of the actual shopping cart
    for (int i = 0; i < shoppingCart.count; i++) {
        CITCafeteriaProduct *product = [shoppingCart objectAtIndex:i];
        total += product.price;
    }
    
    //btnOrder.titleLabel.font = [UIFont fontWithName:@"Lane - Narrow" size:17.0];
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationItem.rightBarButtonItem.title = @"Editar";
    
    txtTotal.text = [NSString stringWithFormat:@"%.2f", total];
    
    tblView.separatorColor = [UIColor colorWithRed:209/255.f green:198/255.f blue:181/255.f alpha:1];
    
    networkIndicator = [UIApplication sharedApplication];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIActions

- (IBAction)buyCart:(id)sender {
    //If the car is empty the transaction is not permitted and we ask the user to select at least one item in order to make the order
    if (shoppingCart.count <= 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Carro vacío" message:@"Por favor selecciona productos para ordenar" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }else{
        networkIndicator.networkActivityIndicatorVisible = YES;
        [self checkPendingOrder];
    }
}

#pragma -mark TableView

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (shoppingCart == nil) {
        return 0;
    }else{
        return shoppingCart.count;
    }
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //We're using a custom row because of design purpose
    CITProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"productCell"];
    CITCafeteriaProduct *product = [shoppingCart objectAtIndex:indexPath.row];
    
    if ( cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CITProductCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.txtName.text = product.name;
    cell.txtPrice.text = [NSString stringWithFormat:@"$%.2f", product.price];

    
    return cell;
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    [tblView setEditing:editing animated:animated];
    
    //Changing the language of the buttons to spanish
    if (editing)
    {
        self.editButtonItem.title = NSLocalizedString(@"Listo", @"Listo");
    }
    else
    {
        self.editButtonItem.title = NSLocalizedString(@"Editar", @"Editar");
    }
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CITCafeteriaProduct *productSelected = [shoppingCart objectAtIndex:indexPath.row];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Producto" message:productSelected.name delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        CITCafeteriaProduct *product = [shoppingCart objectAtIndex:indexPath.row];
        total -= product.price;
        txtTotal.text = [NSString stringWithFormat:@"%.2f", total];
        [shoppingCart removeObjectAtIndex:indexPath.row];
        [tblView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"Eliminar";
}

#pragma mark - NetworkConnection

- (void) performOrder{
    [CITPaymentRequesting performPaymentforCafeteria: selectedCafeteria shoppingCart:shoppingCart forView:self];
}

//In order to know if the transaction is possible, first is needed to check if there is not another order in the queue
- (void) checkPendingOrder{
    [CITPendingOrderRequesting checkPendingOrderforView:self];
}

- (void) successfulPaymentwithId: (int) idPayment{
    idPay = idPayment;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Orden Realizada" message:[NSString stringWithFormat:@"Tu orden fue realizada con el ID:%i", idPayment] delegate:self cancelButtonTitle:@"Continuar" otherButtonTitles: nil];
    [alert show];
    networkIndicator.networkActivityIndicatorVisible = NO;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView.title isEqualToString:@"Orden Realizada"]){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CITViewOrderViewController *nextVC = (CITViewOrderViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ViewOrder"];
        nextVC.idOrder = idPay;
        nextVC.cafeteriaSelected = selectedCafeteria;
        [self presentViewController:nextVC animated:YES completion:nil];
    }
    
    if ([alertView.title isEqualToString:@"Orden pendiente"]){
        if (buttonIndex == 0) {
            networkIndicator.networkActivityIndicatorVisible = YES;
            [CITCancelOrderRequesting cancelPendingOrderwithID:pendingOrderID forView:self];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Orden pendiente" message:@"Por favor pague su orden pendiente antes de continuar." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
    }
}

- (void) pendingOrder:(int)pendingId{
    pendingOrderID = pendingId;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Orden pendiente" message:@"Ya existe una orden pendiente. ¿Deseas cancelarla?" delegate:self cancelButtonTitle:@"Sí" otherButtonTitles: @"No", nil];
    [alert show];
    networkIndicator.networkActivityIndicatorVisible = NO;
}

- (void) orderCancelled{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Orden cancelada" message:@"Se ha cancelado la orden" delegate:nil cancelButtonTitle:@"Continuar" otherButtonTitles: nil];
    [alert show];
    networkIndicator.networkActivityIndicatorVisible = NO;
}

-(void) errorinConnection{
    networkIndicator.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de conexión" message:@"Hubo un error de conexión" delegate:nil cancelButtonTitle:@"Continuar" otherButtonTitles: nil];
    [alert show];
}

@end

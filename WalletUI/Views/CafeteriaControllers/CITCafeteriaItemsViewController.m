//
//  CafeteriaItemsViewController.m
//  WalletUI
//
//  Created by CITA on 1/31/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITCafeteriaItemsViewController.h"

@interface CITCafeteriaItemsViewController ()

@end

@implementation CITCafeteriaItemsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.title = self.cafeteriaSelected.name;
    
    networkIndicator = [UIApplication sharedApplication];

    tblView.separatorColor = [UIColor colorWithRed:209/255.f green:198/255.f blue:181/255.f alpha:1];
    
    shoppingCart = [[NSMutableArray alloc] init];
    
    CITAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    
    itemsCache = delegate.appCache;
    
    NSMutableArray *itemsArray = [itemsCache objectForKey:@"Items"];
    int id = [[itemsCache objectForKey:@"cafeteriaID"] intValue];
    
    if ((itemsArray) && (id == _cafeteriaSelected.id_Categoria)){
        productsCafeteria = itemsArray;
    }else{
        [self performConnection];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated{
    cartBarItem.title = [NSString stringWithFormat:@"Carrito(%lu)", (unsigned long)shoppingCart.count];
}

#pragma -mark NetworkConnection

- (void) performConnection{
    networkIndicator.networkActivityIndicatorVisible = YES;
    [CITMonederosRequesting getMonederoCafeteria:self];
    [CITCafeteriaProductRequesting getProductsofCafeteria:self.cafeteriaSelected forView:self];
}

-(void) successfulConectionwithArrayofObjects: (NSMutableArray *) arrayProducts{
    networkIndicator.networkActivityIndicatorVisible = NO;
    [itemsCache setObject:arrayProducts forKey:@"Items"];
    NSString *stringID = [NSString stringWithFormat:@"%i", _cafeteriaSelected.id_Categoria];
    [itemsCache setObject:stringID forKey:@"cafeteriaID"];
    productsCafeteria = arrayProducts;
    [tblView reloadData];
}

- (void) successfulMonedero:(CITMonedero *)monederoCafeteria{
    float total = monederoCafeteria.saldoIntransferible + monederoCafeteria.saldoTransferible;
    txtTotal.text = [NSString stringWithFormat:@"$%.2f", total];
}

-(void) errorinConnection{
    networkIndicator.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de conexión" message:@"Hubo un error de conexión" delegate:nil cancelButtonTitle:@"Continuar" otherButtonTitles: nil];
    [alert show];
}

#pragma mark - UIActions
- (IBAction)showShoppingCart:(id)sender {
    [self performSegueWithIdentifier:@"ShowShoppingCart" sender:self];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (productsCafeteria == nil) {
        return 0;
    }else{
        return productsCafeteria.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CITProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"productCell"];
    CITCafeteriaProduct *product = [productsCafeteria objectAtIndex:indexPath.row];
    
    if ( cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CITProductCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.txtName.text = product.name;
    cell.txtPrice.text = [NSString stringWithFormat:@"$%.2f", product.price];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CITCafeteriaProduct *nuevoProducto = [productsCafeteria objectAtIndex:indexPath.row];
    [shoppingCart addObject:nuevoProducto];
    cartBarItem.title = [NSString stringWithFormat:@"Carrito(%lu)", (unsigned long)shoppingCart.count];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    CITShoppingCartViewController *nextVC = [segue destinationViewController];
    nextVC.shoppingCart = shoppingCart;
    nextVC.selectedCafeteria = self.cafeteriaSelected;
}


@end

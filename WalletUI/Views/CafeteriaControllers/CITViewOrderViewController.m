//
//  ViewOrderViewController.m
//  WalletUI
//
//  Created by CITA on 2/5/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITViewOrderViewController.h"

@interface CITViewOrderViewController ()

@end

@implementation CITViewOrderViewController

@synthesize idOrder;
@synthesize cafeteriaSelected;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [CITViewControllerEditor changeColorofStatusBarforView:self];
	// Do any additional setup after loading the view.
    
    networkIndicator = [UIApplication sharedApplication];
    
    //btnCancel.titleLabel.font = [UIFont fontWithName:@"Lane - Narrow" size:17.0];
    //btnConfirm.titleLabel.font = [UIFont fontWithName:@"Lane - Narrow" size:17.0];
    
    txtIdOrder.text = [NSString stringWithFormat:@"%i", idOrder];
    
    txtCafeteria.text = [NSString stringWithFormat:@"Tu orden esta apartada en %@", cafeteriaSelected.name];
    
    clickedButton = NO;
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ButtonsActions

- (IBAction)cancellOrder:(id)sender {
    if (!clickedButton) {
        clickedButton = YES;
        networkIndicator.networkActivityIndicatorVisible = YES;
        [CITCancelOrderRequesting cancelOrderwithID:idOrder forView:self];
    }
}

- (IBAction)continuewithView:(id)sender{
    [self goToHome];
}

#pragma mark - NetworkConnection

- (void) orderCancelled{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Orden cancelada" message:@"Tu orden fue cancelada." delegate:self cancelButtonTitle:@"Continuar" otherButtonTitles: nil];
    [alert show];
    networkIndicator.networkActivityIndicatorVisible = NO;
}

-(void) errorinConnection{
    networkIndicator.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de conexión" message:@"Hubo un error de conexión" delegate:nil cancelButtonTitle:@"Continuar" otherButtonTitles: nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView.title isEqualToString:@"Orden cancelada"]){
        [self goToHome];
    }
}

- (void) goToHome{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *myVC = (UITabBarController *)[storyboard instantiateViewControllerWithIdentifier:@"mainView"];
    [self presentViewController:myVC animated:YES completion:nil];
}

@end

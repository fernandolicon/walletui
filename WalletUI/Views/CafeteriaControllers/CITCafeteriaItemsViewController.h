//
//  CafeteriaItemsViewController.h
//  WalletUI
//
//  Created by CITA on 1/31/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CITCafeteria.h"
#import "CITCafeteriaProduct.h"
#import "CITCafeteriaProductRequesting.h"
#import "CITMonedero.h"
#import "CITMonederosRequesting.h"
#import "CITProductCell.h"
#import "CITShoppingCartViewController.h"

@interface CITCafeteriaItemsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>{
    UIApplication *networkIndicator;
    NSMutableArray *productsCafeteria;
    NSMutableArray *shoppingCart;
    __weak IBOutlet UIBarButtonItem *cartBarItem;
    __weak IBOutlet UILabel *txtTotal;
    __weak IBOutlet UITableView *tblView;
    
    NSCache *itemsCache;
}

@property (nonatomic, strong) CITCafeteria *cafeteriaSelected;

- (void) successfulConectionwithArrayofObjects: (NSMutableArray *) arrayProducts;
- (void) successfulMonedero: (CITMonedero *) monederoCafeteria;
- (void) errorinConnection;
- (IBAction)showShoppingCart:(id)sender;

@end

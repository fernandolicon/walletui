//
//  ViewOrderViewController.h
//  WalletUI
//
//  Created by CITA on 2/5/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CITCancelOrderRequesting.h"
#import "CITCafeteria.h"
#import "CITViewControllerEditor.h"

@interface CITViewOrderViewController : UIViewController <UIAlertViewDelegate>{
    
    __weak IBOutlet UILabel *txtIdOrder;
    __weak IBOutlet UILabel *txtCafeteria;
    
    //This BOOL is used to know if the user has pressed once the Cancel button in order to send only one request
    BOOL clickedButton;
    
    UIApplication *networkIndicator;
    __weak IBOutlet UIButton *btnCancel;
    __weak IBOutlet UIButton *btnConfirm;
}

@property int idOrder;
@property (nonatomic, strong) CITCafeteria *cafeteriaSelected;

- (IBAction)cancellOrder:(id)sender;
- (IBAction)continuewithView:(id)sender;

- (void) orderCancelled;
- (void) errorinConnection;

@end

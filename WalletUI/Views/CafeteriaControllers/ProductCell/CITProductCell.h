//
//  ProductCell.h
//  WalletUI
//
//  Created by CITA on 1/31/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CITProductCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *txtName;
@property (weak, nonatomic) IBOutlet UILabel *txtPrice;

@end

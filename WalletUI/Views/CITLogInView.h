//
//  LogInView.h
//  WalletUI
//
//  Created by CITA on 1/27/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CITLogInRequesting.h"
#import "CITUser.h"
#import "CITMiCuentaViewController.h"

@interface CITLogInView : UIViewController{
    
    __weak IBOutlet UITextField *txtUser;
    __weak IBOutlet UITextField *txtPswd;
    UIApplication *networkIndicator;
}

- (IBAction)performConnection:(id)sender;

-(void) successfulConnectionforUser: (NSString *) user andPassword: (NSString *) pass;
-(void) errorWithData;
-(void) errorInConnection;


@end

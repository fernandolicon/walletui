//
//  MisMonederosViewController.h
//  WalletUI
//
//  Created by CITA on 1/28/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CITMonedero.h"
#import "CITMonederosRequesting.h"
#import "CITUser.h"
#import "CITAppDelegate.h"

@interface CITMisMonederosViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>{
    NSArray *monederos;
    double total;
    __weak IBOutlet UITableView *tblViewMonederos;
    __weak IBOutlet UINavigationItem *navBar;
    __weak IBOutlet UILabel *txtTotal;
    UIApplication *networkIndicator;
    NSCache *monederosCache;
}


- (void) successfulConnectionWithArrayofObjects: (NSArray *) arrayMonederos;
- (void) errorinConnection;

@end

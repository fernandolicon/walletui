//
//  TransactionsByPeriodViewController.h
//  WalletUI
//
//  Created by CITA on 1/28/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CITUser.h"
#import "CITTransaction.h"
#import "CITTransactionByPeriodRequesting.h"

@interface CITTransactionsByPeriodViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>{
    
    NSDate *dateTo;
    NSDate *dateFrom;
    NSArray *transactions;
    __weak IBOutlet UITextField *txtFrom;
    __weak IBOutlet UITextField *txtTo;
    __weak IBOutlet UITableView *tblTransactions;
    UIApplication *networkIndicator;
    __weak IBOutlet UIButton *btnSearch;
}

- (IBAction)performConnection:(id)sender;
-(void) successfulConectionwithArrayofObjects: (NSMutableArray *) arrayTransactions;
-(void) errorinConnection;

@end

//
//  TransactionsByPeriodViewController.m
//  WalletUI
//
//  Created by CITA on 1/28/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITTransactionsByPeriodViewController.h"

@interface CITTransactionsByPeriodViewController ()

@end

@implementation CITTransactionsByPeriodViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIDatePicker *datePickerTo = [[UIDatePicker alloc]init];
    datePickerTo.datePickerMode = UIDatePickerModeDate;
    
    //btnSearch.titleLabel.font = [UIFont fontWithName:@"Lane - Narrow" size:17.0];
    
    [datePickerTo setDate:[NSDate date]];
    [datePickerTo addTarget:self action:@selector(updateTextToField:) forControlEvents:UIControlEventValueChanged];
    [txtTo setInputView:datePickerTo ];
    
    UIDatePicker *datePickerFrom = [[UIDatePicker alloc]init];
    datePickerFrom.datePickerMode = UIDatePickerModeDate;
    
    [datePickerFrom setDate:[NSDate date]];
    [datePickerFrom addTarget:self action:@selector(updateTextFromField:) forControlEvents:UIControlEventValueChanged];
    [txtFrom setInputView:datePickerFrom ];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    tblTransactions.separatorColor = [UIColor colorWithRed:209/255.f green:198/255.f blue:181/255.f alpha:1];
    
    networkIndicator = [UIApplication sharedApplication];
}

- (void) dismissKeyboard{
    [txtTo resignFirstResponder];
    [txtFrom resignFirstResponder];
}

#pragma -mark DatePicker

//In order to show Date Picker instead of Keyboard when choosing the text fields, we need this methods, one for each textfield

-(void)updateTextToField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)txtTo.inputView;
    NSDate *dateF = picker.date;
    NSDateFormatter *dateFormatt = [[NSDateFormatter alloc] init];
    [dateFormatt setDateFormat:@"dd/MM/yyyy"];
    
    NSString *date = [dateFormatt stringFromDate:dateF];
    
    txtTo.text = date;
    dateTo = picker.date;
}

-(void)updateTextFromField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)txtFrom.inputView;
    NSDate *dateF = picker.date;
    NSDateFormatter *dateFormatt = [[NSDateFormatter alloc] init];
    [dateFormatt setDateFormat:@"dd/MM/yyyy"];
    
    NSString *date = [dateFormatt stringFromDate:dateF];
    
    txtFrom.text = date;
    dateFrom = picker.date;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark NetworkConnection

- (IBAction)performConnection:(id)sender {
    if ([self dateIsValid]) {
    NSString *user = [CITUser getUserName];
    NSString *to = txtTo.text;
    NSString *from = txtFrom.text;
    networkIndicator.networkActivityIndicatorVisible = YES;
    [CITTransactionByPeriodRequesting getTransactionsforUser:user startingAt:to endingAt:from forView:self];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fecha Incorrecta" message:@"Por favor elija un rango de fechas correcto." delegate:nil cancelButtonTitle:@"Continuar" otherButtonTitles: nil];
        [alert show];
    }
    
}

//We check two things, If date fields have data, and if the first date is later than the second one
- (BOOL) dateIsValid{
    NSString *to = txtTo.text;
    NSString *from = txtFrom.text;
    if (([to isEqualToString:@""]) || ([from isEqualToString:@""])){
        return NO;
    }
    
    if ([dateTo compare:dateFrom] == NSOrderedDescending) {
        return NO;
    }
    
    return YES;
}

-(void) successfulConectionwithArrayofObjects: (NSMutableArray *) arrayTransactions{
    networkIndicator.networkActivityIndicatorVisible = NO;
    CITTransaction *provisional = [arrayTransactions objectAtIndex:0];
    if (provisional.type != nil){
        transactions = arrayTransactions;
    }else{
        transactions = nil;
    }
    [tblTransactions reloadData];
}

-(void) errorinConnection{
    networkIndicator.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de conexión" message:@"Hubo un error de conexión" delegate:nil cancelButtonTitle:@"Continuar" otherButtonTitles: nil];
    [alert show];
}


#pragma -mark TableView

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (transactions == nil) {
        return 0;
    }else{
        return transactions.count;
    }
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"misTransactionsCell"];
    CITTransaction *provisionalTransaction = [transactions objectAtIndex:indexPath.row];
    
    if ( cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"misTransactionsCell"];
    }
    
    cell.textLabel.text = provisionalTransaction.type;
    cell.detailTextLabel.numberOfLines = 2;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Fecha: %@\nHora: %@", provisionalTransaction.date, provisionalTransaction.time];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //cell.textLabel.font = [UIFont fontWithName:@"Lane - Narrow" size:17];
    //cell.detailTextLabel.font = [UIFont fontWithName:@"Lane - Narrow" size:14];
    
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}


@end

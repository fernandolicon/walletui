//
//  TransactionsViewController.h
//  WalletUI
//
//  Created by CITA on 1/28/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CITUser.h"
#import "CITTransactionRequesting.h"
#import "CITTransaction.h"

@interface CITTransactionsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>{
    NSArray *transactions;
    __weak IBOutlet UITableView *tblTransactions;
    UIApplication *networkIndicator;
}

-(void) successfulConectionwithArrayofObjects: (NSMutableArray *) arrayTransactions;
-(void) errorinConnection;

@end

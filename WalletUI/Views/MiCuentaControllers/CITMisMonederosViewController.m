//
//  MisMonederosViewController.m
//  WalletUI
//
//  Created by CITA on 1/28/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITMisMonederosViewController.h"

@interface CITMisMonederosViewController ()

@end

@implementation CITMisMonederosViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    tblViewMonederos.separatorColor = [UIColor colorWithRed:209/255.f green:198/255.f blue:181/255.f alpha:1];
    
    networkIndicator = [UIApplication sharedApplication];
    
    //Checking if the cache has the arrays needed, if not then call them from the server
    
    CITAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    
    monederosCache = delegate.appCache;
    
    NSMutableArray *monederosArray = [monederosCache objectForKey:@"Monederos"];
    
    if (monederosArray){
        monederos = monederosArray;    }else{
        [self performConnection];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark NetworkConnection

- (void) performConnection{
    networkIndicator.networkActivityIndicatorVisible = YES;
    NSString *user = [CITUser getUserName];
    [CITMonederosRequesting getMonederosforUser:user forView:self];
}

//If connection is Successful we make an array of objects
- (void) successfulConnectionWithArrayofObjects:(NSArray *)arrayMonederos{
    //TODO Implement cache in this method
    [monederosCache setObject:arrayMonederos forKey:@"Monederos"];
    
    networkIndicator.networkActivityIndicatorVisible = NO;
    monederos = arrayMonederos;
    [tblViewMonederos reloadData];
}

-(void) errorinConnection{
    networkIndicator.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de conexión" message:@"Hubo un error de conexión" delegate:nil cancelButtonTitle:@"Continuar" otherButtonTitles: nil];
    [alert show];
}

#pragma -mark TableView

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (monederos == nil) {
        return 0;
    }else{
        return monederos.count;
    }
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"misMonederosCell"];
    CITMonedero *provisionalMonedero = [monederos objectAtIndex:indexPath.row];
    
    if ( cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"misMonederosCell"];
    }
    
    cell.textLabel.text = provisionalMonedero.tipoMonedero;
    cell.detailTextLabel.numberOfLines = 2;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Saldo Transferible: %.2f\nSaldo Intransferible: %.2f", provisionalMonedero.saldoTransferible, provisionalMonedero.saldoIntransferible];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    total += provisionalMonedero.saldoIntransferible + provisionalMonedero.saldoTransferible;
    
    //cell.textLabel.font = [UIFont fontWithName:@"Lane - Narrow" size:17];
    //cell.detailTextLabel.font = [UIFont fontWithName:@"Lane - Narrow" size:14];
    
    //If the element is the last one of the table, we change Total text
    if ((indexPath.row + 1) == monederos.count) {
        txtTotal.text = [txtTotal.text stringByAppendingString:[NSString stringWithFormat:@"%.2f", total]];
    }
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

@end

//
//  TransactionsViewController.m
//  WalletUI
//
//  Created by CITA on 1/28/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITTransactionsViewController.h"

@interface CITTransactionsViewController ()

@end

@implementation CITTransactionsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self performConnection];
    
    tblTransactions.separatorColor = [UIColor colorWithRed:209/255.f green:198/255.f blue:181/255.f alpha:1];
    
    networkIndicator = [UIApplication sharedApplication];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark NetworkConnection

- (void) performConnection{
    networkIndicator.networkActivityIndicatorVisible = YES;
    NSString *user = [CITUser getUserName];
    [CITTransactionRequesting getTransactionsforUser:user forView:self];
}

-(void) successfulConectionwithArrayofObjects: (NSMutableArray *) arrayTransactions{
    networkIndicator.networkActivityIndicatorVisible = NO;
    transactions = arrayTransactions;
    [tblTransactions reloadData];
}

-(void) errorinConnection{
    networkIndicator.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de conexión" message:@"Hubo un error de conexión" delegate:nil cancelButtonTitle:@"Continuar" otherButtonTitles: nil];
    [alert show];
}

#pragma -mark TableView

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (transactions == nil) {
        return 0;
    }else{
        return transactions.count;
    }
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"misTransactionsCell"];
    CITTransaction *provisionalTransaction = [transactions objectAtIndex:indexPath.row];
    
    if ( cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"misTransactionsCell"];
    }
    
    cell.textLabel.text = provisionalTransaction.type;
    cell.detailTextLabel.numberOfLines = 2;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Fecha: %@\nHora: %@", provisionalTransaction.date, provisionalTransaction.time];
    
    //cell.textLabel.font = [UIFont fontWithName:@"Lane - Narrow" size:17];
    //cell.detailTextLabel.font = [UIFont fontWithName:@"Lane - Narrow" size:14];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}


@end

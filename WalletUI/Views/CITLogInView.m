//
//  LogInView.m
//  WalletUI
//
//  Created by CITA on 1/27/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITLogInView.h"

@interface CITLogInView ()

@end

@implementation CITLogInView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    networkIndicator = [UIApplication sharedApplication];
}

- (void) dismissKeyboard{
    [txtUser resignFirstResponder];
    [txtPswd resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark Connections

//Method used to connect with the server, calling the correct request class
- (IBAction)performConnection:(id)sender {
    if ([self fieldsAreValid]){
    networkIndicator.networkActivityIndicatorVisible = YES;
    NSString *user = txtUser.text;
    user = [user capitalizedString];
    NSString *password = txtPswd.text;
    [CITLogInRequesting performLogInforController:self withUser:user Password:password];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Datos incorrectos" message:@"Por favor ingrese un usuario y contraseña." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}

//Method called by the request once the connection was successful
-(void) successfulConnectionforUser: (NSString *) user andPassword: (NSString *) pass{
    networkIndicator.networkActivityIndicatorVisible = NO;
    [CITUser setUserName:user];
    [CITUser setPassword:pass];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *myVC = (UITabBarController *)[storyboard instantiateViewControllerWithIdentifier:@"mainView"];
    [self presentViewController:myVC animated:YES completion:nil];
}

//Error with the user name
-(void) errorWithData{
    networkIndicator.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Datos incorrectos" message:@"El usuario o contraseña son incorrectos. Por favor intenta nuevamente." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
}

-(void) errorInConnection{
    networkIndicator.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de conexión" message:@"Hubo un error de conexión" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
}

#pragma mark - CheckTextFields

- (BOOL) fieldsAreValid{
    if (([txtPswd.text length] == 0) || ([txtUser.text length] == 0)) {
        return NO;
    }else{
        return YES;
    }
}

@end

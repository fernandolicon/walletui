//
//  AjustesViewController.h
//  WalletUI
//
//  Created by CITA on 1/30/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CITUser.h"
#import "CITViewControllerEditor.h"

@interface CITAjustesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    NSArray *items;
    __weak IBOutlet UITableView *tblAjustes;
}

@end

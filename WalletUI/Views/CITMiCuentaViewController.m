//
//  MiCuentaViewController.m
//  WalletUI
//
//  Created by CITA on 1/27/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITMiCuentaViewController.h"

@interface CITMiCuentaViewController ()

@end

@implementation CITMiCuentaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [CITViewControllerEditor changeNavigationBarColorforView:self];
    
    items = [[NSArray alloc] initWithObjects:@"Mis Monederos", @"Transacciones", @"Transacciones por periodo", nil];
    self.tableView.separatorColor = [UIColor colorWithRed:209/255.f green:198/255.f blue:181/255.f alpha:1];
    /*
    UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 40)];
    [myLabel setFont:[UIFont fontWithName:@"Lane - Narrow" size:25]];
    [myLabel setTextColor:[UIColor whiteColor]];
    [myLabel setText:@"Mi Cuenta"];
    [self.navigationController.navigationBar.topItem setTitleView:myLabel];*/
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark - TableView

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return items.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"miCuentaCell"];
    
    if ( cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"miCuentaCell"];
    }
    
    cell.textLabel.text = [items objectAtIndex:indexPath.row];
    
    //cell.textLabel.font = [UIFont fontWithName:@"Lane - Narrow" size:17];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            [self performSegueWithIdentifier:@"MisMonederos" sender:self];
            break;
        case 1:
            [self performSegueWithIdentifier:@"Transactions" sender:self];
            break;
        case 2:
            [self performSegueWithIdentifier:@"TransactionsbyPeriod" sender:self];
            break;
        default:
            break;
    }
}

@end

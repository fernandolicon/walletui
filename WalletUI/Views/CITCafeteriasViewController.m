//
//  CafeteriasViewController.m
//  WalletUI
//
//  Created by CITA on 1/30/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITCafeteriasViewController.h"

@interface CITCafeteriasViewController ()

@end

@implementation CITCafeteriasViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    app = [UIApplication sharedApplication];
    
    [CITViewControllerEditor changeNavigationBarColorforView:self];
    
    self.tableView.separatorColor = [UIColor colorWithRed:209/255.f green:198/255.f blue:181/255.f alpha:1];
    
    [self performConnection];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark NetworkConnection

- (void) performConnection{
    app.networkActivityIndicatorVisible = YES;
    [CITCafeteriaRequesting getCafeteriasforView:self];
}

//If connection is Successful we make an array of objects
- (void) successfulConnectionWithArrayofObjects:(NSArray *)arrayCafeterias{
    app.networkActivityIndicatorVisible = NO;
    cafeteriasArray = arrayCafeterias;
    [self.tableView reloadData];
}

-(void) errorinConnection{
    app.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de conexión" message:@"Hubo un error de conexión" delegate:nil cancelButtonTitle:@"Continuar" otherButtonTitles: nil];
    [alert show];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return cafeteriasArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CITCafeteria *provisional = [cafeteriasArray objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cafeteriaCell"];
    
    if ( cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cafeteriaCell"];
    }
    
    cell.textLabel.text = provisional.name;
    
    //cell.textLabel.font = [UIFont fontWithName:@"Lane - Narrow" size:17];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectedCafeteria = [cafeteriasArray objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"PresentCafeteria" sender:self];
}


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    CITCafeteriaItemsViewController *presentCafeteria = [segue destinationViewController];
    presentCafeteria.cafeteriaSelected = selectedCafeteria;
    
}



@end

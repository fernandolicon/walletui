//
//  main.m
//  WalletUI
//
//  Created by CITA on 1/27/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CITAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CITAppDelegate class]));
    }
}

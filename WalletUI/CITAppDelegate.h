//
//  AppDelegate.h
//  WalletUI
//
//  Created by CITA on 1/27/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Requests/CITLogInRequesting.h"
#import "Classes/CITUser.h"

@interface CITAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSCache *appCache;

- (void) errorInConnection;
- (void) errorWithUser;


@end

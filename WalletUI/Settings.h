//
//  Settings.h
//  ExampleHTTPRequest
//
//  Created by CITA on 1/23/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//
//  Here we're going to storage Strings used in all the app

#ifndef ExampleHTTPRequest_Settings_h
#define ExampleHTTPRequest_Settings_h

//Base URL for Web Service connections
#define WSBaseUrl @"http://cita.chi.itesm.mx/monedero/ws/"

#endif

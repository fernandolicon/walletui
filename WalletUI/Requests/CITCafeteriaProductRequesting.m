//
//  CafeteriaProductRequest.m
//  ExampleHTTPRequest
//
//  Created by CITA on 1/24/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITCafeteriaProductRequesting.h"

@implementation CITCafeteriaProductRequesting

+(void) getProductsofCafeteria: (CITCafeteria *) cafeteriatoList forView: (CITCafeteriaItemsViewController *) controller{
    
    NSString *id_Cafeteria = [NSString stringWithFormat:@"%i", cafeteriatoList.id_Categoria];
    
    //Setting the url, WSBaseUrl comes fomr Settings.h
    NSString *url = [NSString stringWithFormat:@"%@cafeterias/buscarProductos?idCafeteria=%@",WSBaseUrl, id_Cafeteria];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    //Here we set the request as an HTTP type and we set the response as a XMLDictionary, in order to get
    //a dictionary with all the elements of the XML and use it at our will
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFXMLDictionaryResponseSerializer serializer]];
    
    //    Setting special headers needed to perform the connection
    //    the Header Content-Type is needed in order to the POST working correctly
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"movil.monedero" password:@"m0n3M0v1l"];
    
    //Perform the GET sending no parameters.
    // We're going to return an array of objects
    // The actvity is going to do whatever it needs with that array.
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSMutableArray *ProductsArray = [[NSMutableArray alloc] init];
        NSArray *DictionaryArray =  [responseObject objectForKey:@"conceptoWS"];
        //If DictionaryArray.count is equal to 1 it means that there's only one product in the list, so we can't perform the for
        if (DictionaryArray.count > 1) {
            for (NSDictionary *provisionalDicc in DictionaryArray) {
                CITCafeteriaProduct *provisional = [[CITCafeteriaProduct alloc] initWithDictionary:provisionalDicc];
                [ProductsArray addObject:provisional];
            }
        }else{
            CITCafeteriaProduct *provisional = [[CITCafeteriaProduct alloc] initWithDictionary:[responseObject objectForKey:@"conceptoWS"]];
            [ProductsArray addObject:provisional];
        }
        
        [controller successfulConectionwithArrayofObjects:ProductsArray];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [controller errorinConnection];
    }];
}

@end

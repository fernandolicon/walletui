//
//  PendingOrderRequest.h
//  WalletUI
//
//  Created by CITA on 2/6/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CITUser.h"
#import "CITShoppingCartViewController.h"

@interface CITPendingOrderRequesting : NSObject

+ (void) checkPendingOrderforView:(CITShoppingCartViewController *)controller;



@end

//
//  LogInRequest.h
//  ExampleHTTPRequest
//
//  Created by CITA on 1/20/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//
//  We are using the library of AFNetworking 2,0,0 developed by Matt Thompson http://afnetworking.com/
//  GITHub link: https://github.com/AFNetworking
//  XMLDictionary 1.3 developed by Nick Lockwood http://www.charcoaldesign.co.uk/source/cocoa#xml-dictionary
//  GITHub link: https://github.com/nicklockwood/XMLDictionary
//  AFXMLDictionarySerializer developed by Thiago Perez
//  GITHub link: https://github.com/ideaismobile/AFXMLDictionarySerializer
//

#import <Foundation/Foundation.h>
#import "CITLogInView.h"
#import "CITUser.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFXMLDictionaryResponseSerializer.h"
#import "XMLDictionary.h"
#import "Settings.h"
#import "CITAppDelegate.h"
@class CITLogInView;
@class CITAppDelegate;

@interface CITLogInRequesting : NSObject

+ (void) performLogInforController: (CITLogInView *) controller withUser: (NSString *) username Password: (NSString *) password;

+ (void) checkConnectionforActiveUserforView: (CITAppDelegate *) controller;

@end

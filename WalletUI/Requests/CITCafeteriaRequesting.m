//
//  CafeteriaRequest.m
//  ExampleHTTPRequest
//
//  Created by CITA on 1/24/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITCafeteriaRequesting.h"

@implementation CITCafeteriaRequesting

+(void) getCafeteriasforView: (CITCafeteriasViewController *) controller{
    //Setting the url, WSBaseUrl comes fomr Settings.h
    NSString *url = [NSString stringWithFormat:@"%@cafeterias/listar",WSBaseUrl];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    //Here we set the request as an HTTP type and we set the response as a XMLDictionary, in order to get
    //a dictionary with all the elements of the XML and use it at our will
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFXMLDictionaryResponseSerializer serializer]];
    
    //    Setting special headers needed to perform the connection
    //    the Header Content-Type is needed in order to the POST working correctly
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"movil.monedero" password:@"m0n3M0v1l"];
    
    //Perform the GET sending no parameters.
    // We're going to return an array of objects
    // The actvity is going to do whatever it needs with that array.
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSMutableArray *CafeteriasArray = [[NSMutableArray alloc] init];
        NSArray *DictionaryArray =  [responseObject objectForKey:@"categoriaWS"];
        
        for (NSDictionary *provisionalDicc in DictionaryArray) {
            CITCafeteria *provisional = [[CITCafeteria alloc] initWithDictionary:provisionalDicc];
            [CafeteriasArray addObject:provisional];
        }
        
        [controller successfulConnectionWithArrayofObjects:CafeteriasArray];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [controller errorinConnection];
    }];
}

@end

//
//  CafeteriaRequest.h
//  ExampleHTTPRequest
//
//  Created by CITA on 1/24/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CITCafeteriasViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFXMLDictionaryResponseSerializer.h"
#import "XMLDictionary.h"
#import "Settings.h"
#import "CITCafeteria.h"
@class CITCafeteriasViewController;

@interface CITCafeteriaRequesting : NSObject

+(void) getCafeteriasforView: (CITCafeteriasViewController *) controller;

@end

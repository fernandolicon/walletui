//
//  MonederosRequest.h
//  ExampleHTTPRequest
//
//  Created by CITA on 1/21/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CITMisMonederosViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFXMLDictionaryResponseSerializer.h"
#import "XMLDictionary.h"
#import "Settings.h"
#import "CITMonedero.h"
#import "CITUser.h"
#import "CITCafeteriaItemsViewController.h"
@class CITMisMonederosViewController;
@class CITCafeteriaItemsViewController;

@interface CITMonederosRequesting : NSObject

+(void) getMonederosforUser: (NSString *) user forView: (CITMisMonederosViewController *) controller;
+(void) getMonederoCafeteria: (CITCafeteriaItemsViewController *) controller;

@end

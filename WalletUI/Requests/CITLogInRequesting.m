//
//  LogInRequest.m
//  ExampleHTTPRequest
//
//  Created by CITA on 1/20/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITLogInRequesting.h"

@implementation CITLogInRequesting



+ (void) performLogInforController: (CITLogInView *) controller withUser: (NSString *) username Password: (NSString *) password{
    
    //Setting the url, WSBaseUrl comes fomr Settings.h
    NSString *url = [NSString stringWithFormat:@"%@usuario/autenticar?usuario=%@&password=%@", WSBaseUrl, username, password];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    //Here we set the request as an HTTP type and we set the response as a XMLDictionary, in order to get
    //a dictionary with all the elements of the XML and use it at our will
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFXMLDictionaryResponseSerializer serializer]];
    
    //    Setting special headers needed to perform the connection
    //    the Header Content-Type is needed in order to the POST working correctly
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"movil.monedero" password:@"m0n3M0v1l"];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //Perform the POST sending no parameters, we don't need them
    [manager POST:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        
        //If the count of response object is equal to 1 it means that we received an error from the WS
        //this will send an error message to the user
        
        if (responseObject.count == 1) {
            [controller errorWithData];
        }else{
            [controller successfulConnectionforUser:username andPassword:password];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [controller errorInConnection];
    }];
    
}

+ (void) checkConnectionforActiveUserforView:(CITAppDelegate *)controller{
    
    //Get the Active User name and password
    NSString *username = [CITUser getUserName];
    NSString *password = [CITUser getPassword];
    
    //Setting the url, WSBaseUrl comes fomr Settings.h
    NSString *url = [NSString stringWithFormat:@"%@usuario/autenticar?usuario=%@&password=%@", WSBaseUrl, username, password];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    //Here we set the request as an HTTP type and we set the response as a XMLDictionary, in order to get
    //a dictionary with all the elements of the XML and use it at our will
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFXMLDictionaryResponseSerializer serializer]];
    
    //    Setting special headers needed to perform the connection
    //    the Header Content-Type is needed in order to the POST working correctly
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"movil.monedero" password:@"m0n3M0v1l"];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //Perform the POST sending no parameters, we don't need them
    [manager POST:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        
        //If the count of response object is equal to 1 it means that we received an error from the WS
        //this will send an error message to the user
        if (responseObject.count == 1) {
            [controller errorWithUser];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [controller errorInConnection];
    }];
    
}

@end

//
//  PaymentRequest.m
//  WalletUI
//
//  Created by CITA on 2/4/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITPaymentRequesting.h"

@implementation CITPaymentRequesting

+ (void) performPaymentforCafeteria:(CITCafeteria *)cafeteria shoppingCart:(NSMutableArray *)shoppingCart forView:(CITShoppingCartViewController *)controller{
    NSString *username = [CITUser getUserName];
    NSString *productsId = @"";
    
    for (int i = 0; i < shoppingCart.count; i++) {
        CITCafeteriaProduct *provisional = [shoppingCart objectAtIndex:i];
        productsId = [productsId stringByAppendingString:[NSString stringWithFormat:@"%i", provisional.idConcepto]];
        if (i != (shoppingCart.count - 1)) {
            productsId = [productsId stringByAppendingString:@","];
            }
    }
    
    //Setting the url, WSBaseUrl comes fomr Settings.h
    NSString *url = [NSString stringWithFormat:@"%@venta/levantarOrden?idUsuario=%@&idCafeteria=%i&idProductos=%@", WSBaseUrl, username, cafeteria.id_Categoria, productsId];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    //Here we set the request as an HTTP type and we set the response as a XMLDictionary, in order to get
    //a dictionary with all the elements of the XML and use it at our will
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFXMLDictionaryResponseSerializer serializer]];
    
    //    Setting special headers needed to perform the connection
    //    the Header Content-Type is needed in order to the POST working correctly
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"movil.monedero" password:@"m0n3M0v1l"];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //Perform the POST sending no parameters, we don't need them
    [manager POST:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        if ([[responseObject objectForKey:@"mensaje"] isEqualToString:@"Transaccion exitosa"]) {
            int idPayment = [[responseObject objectForKey:@"id"] intValue];
            [controller successfulPaymentwithId: idPayment];
        }else{
                [controller errorinConnection];
            }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [controller errorinConnection];
    }];
}

@end

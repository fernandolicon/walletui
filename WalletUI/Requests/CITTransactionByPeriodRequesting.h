//
//  TransactionRequestByPeriod.h
//  ExampleHTTPRequest
//
//  Created by CITA on 1/24/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CITTransactionsByPeriodViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFXMLDictionaryResponseSerializer.h"
#import "XMLDictionary.h"
#import "Settings.h"
#import "CITTransaction.h"
@class CITTransactionsByPeriodViewController;

@interface CITTransactionByPeriodRequesting : NSObject

+(void) getTransactionsforUser: (NSString *) user startingAt:(NSString *) startDate endingAt: (NSString *) endDate forView: (CITTransactionsByPeriodViewController *) controller;

@end

//
//  CancelOrderRequest.h
//  WalletUI
//
//  Created by CITA on 2/5/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CITUser.h"
#import "CITCafeteria.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFXMLDictionaryResponseSerializer.h"
#import "XMLDictionary.h"
#import "Settings.h"
#import "CITViewOrderViewController.h"
#import "CITShoppingCartViewController.h"
@class CITViewOrderViewController;
@class CITShoppingCartViewController;

@interface CITCancelOrderRequesting : NSObject

+ (void) cancelOrderwithID: (int) idOrder forView:(CITViewOrderViewController *)controller;
+ (void) cancelPendingOrderwithID: (int) idOrder forView:(CITShoppingCartViewController *)controller;


@end

//
//  TransactionRequest.h
//  ExampleHTTPRequest
//
//  Created by CITA on 1/23/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CITTransactionsViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFXMLDictionaryResponseSerializer.h"
#import "XMLDictionary.h"
#import "Settings.h"
#import "CITTransaction.h"
@class CITTransactionsViewController;

@interface CITTransactionRequesting : NSObject

+(void) getTransactionsforUser: (NSString *) user forView: (CITTransactionsViewController *) controller;

@end

//
//  PaymentRequest.h
//  WalletUI
//
//  Created by CITA on 2/4/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CITUser.h"
#import "CITCafeteria.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFXMLDictionaryResponseSerializer.h"
#import "XMLDictionary.h"
#import "Settings.h"
#import "CITShoppingCartViewController.h"
@class CITShoppingCartViewController;

@interface CITPaymentRequesting : NSObject

+ (void) performPaymentforCafeteria:(CITCafeteria *)cafeteria shoppingCart:(NSMutableArray *)shoppingCart forView:(CITShoppingCartViewController *)controller;

@end

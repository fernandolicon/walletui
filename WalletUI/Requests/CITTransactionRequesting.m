//
//  TransactionRequest.m
//  ExampleHTTPRequest
//
//  Created by CITA on 1/23/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import "CITTransactionRequesting.h"

@implementation CITTransactionRequesting

+(void) getTransactionsforUser: (NSString *) user forView: (CITTransactionsViewController *) controller{
    
    //Setting the url, WSBaseUrl comes fomr Settings.h
    NSString *url = [NSString stringWithFormat:@"%@transaccion/listarUltimas?usuario=%@",WSBaseUrl, user];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    //Here we set the request as an HTTP type and we set the response as a XMLDictionary, in order to get
    //a dictionary with all the elements of the XML and use it at our will
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFXMLDictionaryResponseSerializer serializer]];
    
    //    Setting special headers needed to perform the connection
    //    the Header Content-Type is needed in order to the POST working correctly
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"movil.monedero" password:@"m0n3M0v1l"];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //Perform the GET sending no parameters.
    // We're going to return an array of objects
    // The actvity is going to do whatever it needs with that array.
    //We need to assure if the Web Service is returning an Array of Dictioanries or a Single Dictionary, that's why the If stands for
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSMutableArray *TransactionsArray = [[NSMutableArray alloc] init];
        NSArray *DictionaryArray =  [responseObject objectForKey:@"transaccionWSs"];
        if (DictionaryArray.count > 0) {
                for (NSDictionary *provisionalDicc in DictionaryArray) {
                CITTransaction *provisional = [[CITTransaction alloc] initWithDictionary:provisionalDicc];
                [TransactionsArray addObject:provisional];
            }
        }else{
            CITTransaction *provisional = [[CITTransaction alloc] initWithDictionary:[responseObject objectForKey:@"transaccionWS"]];
            [TransactionsArray addObject:provisional];
        }
        
        
        [controller successfulConectionwithArrayofObjects:TransactionsArray];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [controller errorinConnection];
    }];
}

@end

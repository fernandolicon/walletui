//
//  CafeteriaProductRequest.h
//  ExampleHTTPRequest
//
//  Created by CITA on 1/24/14.
//  Copyright (c) 2014 CITA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CITCafeteriaItemsViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFXMLDictionaryResponseSerializer.h"
#import "XMLDictionary.h"
#import "Settings.h"
#import "CITCafeteriaProduct.h"
#import "CITCafeteria.h"
@class CITCafeteriaItemsViewController;

@interface CITCafeteriaProductRequesting : NSObject

+(void) getProductsofCafeteria: (CITCafeteria *) cafeteriatoList forView: (CITCafeteriaItemsViewController *) controller;

@end

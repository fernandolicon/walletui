WalletUI for iOS 1.0.0

This app was made by the:
Centro de Investigación y Tecnología Aplicada
Instituto Tecnológico y de Estudios Superiores de Monterrey Campus Chihuhua
Started: 15/01/2014
Finished: 7/02/2014

Work Team:
Luis Fernando Mata Licón - iOS Developer fernando.mata.licon@gmail.com

To check the android version go to: https://bitbucket.org/enriquealo/wallet

This project uses Cocoapods 0.29.0

And the next libraries:

AFNetworking 2,0,0 developed by Matt Thompson http://afnetworking.com/
GITHub link: https://github.com/AFNetworking

XMLDictionary 1.3 developed by Nick Lockwood http://www.charcoaldesign.co.uk/source/cocoa#xml-dictionary
GITHub link: https://github.com/nicklockwood/XMLDictionary

AFXMLDictionarySerializer developed by Thiago Perez
GITHub link: https://github.com/ideaismobile/AFXMLDictionarySerializer

This app belongs to the Centro de Investigación y Tecnología Aplicada from the Instituto Tecnológico y de Estudios Superiores de Monterrey Campus Chihuahua.